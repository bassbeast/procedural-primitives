using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPrimitiveHelperMethods
{
	void createNewPrimitive(ref GameObject[] pObject, ref Mesh[] mesh, Transform transform, int index);
	void deleteOldPrimitive(ref GameObject[] pObject, ref Mesh[] mesh, ref int[] Triangles, ref Vector3[] Vertices);
	Vector3 calculateNormal(ref Mesh mesh, int index);
	void recalculateNormals(ref Mesh mesh);
}
