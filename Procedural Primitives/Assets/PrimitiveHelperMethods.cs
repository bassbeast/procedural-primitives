using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* \brief This is a helper class for generating primitives
* \detailed This class implements the IprimitiveHelperMethods to
* help with creating and deleting primitives.
* \author Jacob Picard
* \date 2022
*/
public class PrimitiveHelperMethods : IPrimitiveHelperMethods
{
	/**
	* \brief Creates a new piece of a primitive object.
	* \param pObject The array of objects that will be created.
	* \param mesh The mesh of the objects that will be created.
	* \param transform The parents transform, this crates the mesh in the right place in 3D space.
	* \param index The index in the list of pObjects to create a new object.
	*/
	void IPrimitiveHelperMethods.createNewPrimitive(ref GameObject[] pObject, ref Mesh[] mesh, Transform transform, int index)
	{
		pObject[index] = new GameObject("Primitive Piece " + index);
		pObject[index].transform.SetParent(transform);
		pObject[index].AddComponent<MeshRenderer>().sharedMaterial = new Material (Shader.Find("Standard"));
		pObject[index].AddComponent<MeshFilter>();
		pObject[index].GetComponent<MeshFilter>().sharedMesh = new Mesh();
		mesh[index] = pObject[index].GetComponent<MeshFilter>().sharedMesh;
	}
	
	/**
	* \brief Deletes an entire primitive and all of its pieces.
	* \param pObject The array of objects or pieces to delete.
	* \param mesh The array of meshes to delete.
	* \param Triangles An array containing the list of triangles to delete.
	* \param Vertices An array containing the list of vertices to delete.
	*/
	void IPrimitiveHelperMethods.deleteOldPrimitive(ref GameObject[] pObject, ref Mesh[] mesh, ref int[] Triangles, ref Vector3[] Vertices)
	{
		if(pObject != null || mesh != null)
		{
			for(int i = 0; i < mesh.Length; i++)
			{
				mesh[i].Clear();
				Mesh.DestroyImmediate(mesh[i]);
				GameObject.DestroyImmediate(pObject[i]);
			}
			Array.Resize<Mesh>(ref mesh, 0);
			Array.Resize<GameObject>(ref pObject, 0);
			Array.Resize<int>(ref Triangles, 0);
			Array.Resize<Vector3>(ref Vertices, 0);
		}
	}
	
	Vector3 IPrimitiveHelperMethods.calculateNormal(ref Mesh mesh, int index)
	{
		return Vector3.Cross(mesh.vertices[index + 2] - mesh.vertices[index], mesh.vertices[index + 1] - mesh.vertices[index]).normalized;
	}
	
	void IPrimitiveHelperMethods.recalculateNormals(ref Mesh mesh)
	{
		for(int i = 0; i < mesh.normals.Length; i+= 3)
		{
			
		}
	}
}
