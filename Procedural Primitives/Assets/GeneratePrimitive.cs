using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GeneratePrimitive : PrimitiveBase
{
	public void GenerateCube()
	{
		if (pieces == Pieces.onePiece)
		{
			GameObject cube = new GameObject("Cube");
			cube.AddComponent<Cube>();
			cube.GetComponent<Cube>().PHM = new PrimitiveHelperMethods();
			cube.GetComponent<Cube>().pieces = Cube.Pieces.onePiece;
			cube.GetComponent<Cube>().onePieceCubeUpdate(xSize, ySize, zSize, levelOfDetail);
		}
		if (pieces == Pieces.sixPiece)
		{
			GameObject cube = new GameObject("Cube");
			cube.AddComponent<Cube>();
			cube.GetComponent<Cube>().PHM = new PrimitiveHelperMethods();
			cube.GetComponent<Cube>().pieces = Cube.Pieces.sixPiece;
			cube.GetComponent<Cube>().sixPieceCubeUpdate(xSize, ySize, zSize, levelOfDetail);
		}
		if(pieces == Pieces.manyPiece)
		{
			GameObject cube = new GameObject("Cube");
			cube.AddComponent<Cube>();
			cube.GetComponent<Cube>().PHM = new PrimitiveHelperMethods();
			cube.GetComponent<Cube>().pieces = Cube.Pieces.manyPiece;
			cube.GetComponent<Cube>().manyPieceCubeUpdate(xSize, ySize, zSize, levelOfDetail);
		}
	}
	
	public void GeneratePyramid()
	{
		if (pieces == Pieces.onePiece)
		{
			GameObject pyramid = new GameObject("Pyramid");
			pyramid.AddComponent<Pyramid>();
			pyramid.GetComponent<Pyramid>().pieces = Pyramid.Pieces.onePiece;
			pyramid.GetComponent<Pyramid>().onePiecePyramidUpdate(xSize, ySize, zSize, levelOfDetail);
		}
		if (pieces == Pieces.fivePiece)
		{
			GameObject pyramid = new GameObject("Pyramid");
			pyramid.AddComponent<Pyramid>();
			pyramid.GetComponent<Pyramid>().pieces = Pyramid.Pieces.fivePiece;
			pyramid.GetComponent<Pyramid>().fivePiecePyramidUpdate(xSize, ySize, zSize, levelOfDetail);
		}
		if(pieces == Pieces.manyPiece)
		{
			GameObject pyramid = new GameObject("Pyramid");
			pyramid.AddComponent<Pyramid>();
			pyramid.GetComponent<Pyramid>().pieces = Pyramid.Pieces.manyPiece;
			pyramid.GetComponent<Pyramid>().manyPiecePyramidUpdate(xSize, ySize, zSize, levelOfDetail);
		}
	}
}
