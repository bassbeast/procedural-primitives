using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class Builder
{
    static string[] sceneList = { "Assets/Scenes/SampleScene.unity" };

    public static void BuildLinuxHeadless()
    {
        var report = BuildPipeline.BuildPlayer(new BuildPlayerOptions()
        {
            locationPathName = "../target/server",
            scenes = sceneList,
            target = BuildTarget.StandaloneLinux64,
            //subtarget = (int)StandaloneBuildSubtarget.Server
        });

        Debug.Log($"Build result: {report.summary.result}, {report.summary.totalErrors} errors");
        if (report.summary.totalErrors > 0)
            EditorApplication.Exit(1);
    }
}