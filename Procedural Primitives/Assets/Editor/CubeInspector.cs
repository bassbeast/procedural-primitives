using System.Collections;
using System.Collections.Generic;

using UnityEditor;

[CustomEditor(typeof(Cube))]
public class CubeInspector : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		Cube cubeObject = (Cube)target;

		EditorGUI.BeginChangeCheck();
		
		cubeObject.xSize = EditorGUILayout.Slider("X Size", cubeObject.xSize, 1, 50);
		cubeObject.ySize = EditorGUILayout.Slider("Y Size", cubeObject.ySize, 1, 50);
		cubeObject.zSize = EditorGUILayout.Slider("Z Size", cubeObject.zSize, 1, 50);
		cubeObject.levelOfDetail = EditorGUILayout.IntSlider("Cube Level Of Detail",
		cubeObject.levelOfDetail, 2 ,50);
		
		cubeObject.pieces = (Cube.Pieces)EditorGUILayout.EnumPopup(
		"How many pieces?", cubeObject.pieces);
		
		if(EditorGUI.EndChangeCheck())
		{
			if(cubeObject.pieces == Cube.Pieces.onePiece)
			{
				cubeObject.onePieceCubeUpdate(cubeObject.xSize,cubeObject.ySize, 
				cubeObject.zSize, cubeObject.levelOfDetail);
			}
			if(cubeObject.pieces == Cube.Pieces.sixPiece)
			{
				cubeObject.sixPieceCubeUpdate(cubeObject.xSize,cubeObject.ySize, 
				cubeObject.zSize, cubeObject.levelOfDetail);
			}
			if(cubeObject.pieces == Cube.Pieces.manyPiece)
			{
				cubeObject.manyPieceCubeUpdate(cubeObject.xSize,cubeObject.ySize, 
				cubeObject.zSize, cubeObject.levelOfDetail);
			}
		}
		EditorGUILayout.LabelField("Time to Generate: ", cubeObject.timeElapsed);
		EditorGUILayout.LabelField("Number Of Vertices: ", cubeObject.numberOfVertices.ToString());
		EditorGUILayout.LabelField("Number Of Triangles: ", cubeObject.numberOfTriangles.ToString());
	}
}
