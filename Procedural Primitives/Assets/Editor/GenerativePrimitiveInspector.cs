using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GeneratePrimitive))]
public class GenerativePrimitiveInspector : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		GeneratePrimitive GP = (GeneratePrimitive)target;
		
		EditorGUILayout.BeginFoldoutHeaderGroup(true, "Pyramid");
		GP.xSize = EditorGUILayout.Slider("X Size", GP.xSize, 1, 50);
		GP.ySize = EditorGUILayout.Slider("Y Size", GP.ySize, 1, 50);
		GP.zSize = EditorGUILayout.Slider("Z Size", GP.zSize, 1, 50);
		GP.levelOfDetail = EditorGUILayout.IntSlider("Pyramid Level Of Detail",
		GP.levelOfDetail, 2, 50);
		
		GP.pieces = (GeneratePrimitive.Pieces)EditorGUILayout.EnumPopup(
		"How many pieces?", GP.pieces);
		
		if (GUILayout.Button("Generate Pyramid"))
		{
			GP.GeneratePyramid();
		}
		
		EditorGUILayout.EndFoldoutHeaderGroup();
		
		EditorGUILayout.BeginFoldoutHeaderGroup(true, "Cube");
		GP.xSize = EditorGUILayout.Slider("X Size", GP.xSize, 1, 50);
		GP.ySize = EditorGUILayout.Slider("Y Size", GP.ySize, 1, 50);
		GP.zSize = EditorGUILayout.Slider("Z Size", GP.zSize, 1, 50);
		GP.levelOfDetail = EditorGUILayout.IntSlider("Cube Level Of Detail",
		GP.levelOfDetail, 2, 50);
		
		GP.pieces = (GeneratePrimitive.Pieces)EditorGUILayout.EnumPopup(
		"How many pieces?", GP.pieces);
		
		if (GUILayout.Button("Generate Cube"))
		{
			GP.GenerateCube();
		}
		EditorGUILayout.EndFoldoutHeaderGroup();
	}
}