using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pyramid))]
public class PyramidInspector : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		Pyramid pyramidObject = (Pyramid)target;
		
		EditorGUI.BeginChangeCheck();
		
		pyramidObject.xSize = EditorGUILayout.Slider("X Size", pyramidObject.xSize, 1, 50);
		pyramidObject.ySize = EditorGUILayout.Slider("Y Size", pyramidObject.ySize, 1, 50);
		pyramidObject.zSize = EditorGUILayout.Slider("Z Size", pyramidObject.zSize, 1, 50);
		pyramidObject.levelOfDetail = EditorGUILayout.IntSlider("Pyramid Level Of Detail",
		pyramidObject.levelOfDetail, 2 ,50);
		
		pyramidObject.pieces = (Cube.Pieces)EditorGUILayout.EnumPopup(
		"How many pieces?", pyramidObject.pieces);
		
		if(EditorGUI.EndChangeCheck())
		{
			if(pyramidObject.pieces == Pyramid.Pieces.onePiece)
			{
				pyramidObject.onePiecePyramidUpdate(pyramidObject.xSize, pyramidObject.ySize,
				pyramidObject.zSize, pyramidObject.levelOfDetail);
			}
			if(pyramidObject.pieces == Pyramid.Pieces.fivePiece)
			{
				pyramidObject.fivePiecePyramidUpdate(pyramidObject.xSize, pyramidObject.ySize,
				pyramidObject.zSize, pyramidObject.levelOfDetail);
			}
			if(pyramidObject.pieces == Pyramid.Pieces.manyPiece)
			{
				pyramidObject.manyPiecePyramidUpdate(pyramidObject.xSize, pyramidObject.ySize,
				pyramidObject.zSize, pyramidObject.levelOfDetail);
			}
		}
		EditorGUILayout.LabelField("Time to Generate: ", pyramidObject.timeElapsed);
		EditorGUILayout.LabelField("Number Of Vertices: ", pyramidObject.numberOfVertices.ToString());
		EditorGUILayout.LabelField("Number Of Triangles: ", pyramidObject.numberOfTriangles.ToString());
	}
}
