using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimitiveBase : MonoBehaviour
{
	[Range(1, 50)]
	public float xSize, ySize, zSize;
	[Range(2, 50)]
	public int levelOfDetail;
	
	public Stopwatch stopWatch;
	
	public bool Collider, RigidBody;
	
	public Pieces pieces;
	
	public enum Pieces
	{
		onePiece, fivePiece, sixPiece, manyPiece
	}
	
	public int numberOfVertices
	{
		get; protected set;
	}
	public int numberOfTriangles
	{
		get; protected set;
	}
	public string timeElapsed
	{
		get; protected set;
	}
	protected GameObject[] primitiveObject;
	protected Mesh[] mesh;
	protected int[] Triangles;
	protected Vector3[] Vertices;
	
	public void startStopwatch()
	{
		stopWatch = new Stopwatch();
		stopWatch.Start();
	}
	public void stopStopwatch()
	{
		stopWatch.Stop();
		this.timeElapsed = stopWatch.Elapsed.ToString();
	}
}
