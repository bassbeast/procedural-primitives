using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEditor;

/**
* \brief Creates different types of primitiveObject.
* \detailed This class creates primitiveObject in different ways using more or less
* vertices in order to optimize for different situations.
* \author Jacob Picard
* \date 2022
*/
public class Cube : PrimitiveBase
{
	public IPrimitiveHelperMethods PHM;
	
	/**
	* \brief Create a cube with one mesh.
	* \param xSize The length of the cube on the x Axis.
	* \param ySize The length of the cube on the y Axis.
	* \param zSize The length of the cube on the z Axis.
	* \param levelOfDetail This controls how many vertices the cube will be made up of.
	*/
	public void onePieceCubeUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject, ref mesh, ref Triangles, ref Vertices);
		primitiveObject = new GameObject[1];
		mesh = new Mesh[1];
		PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, 0);
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		numberOfVertices = 2 * (levelOfDetail * levelOfDetail) + 2 * ((levelOfDetail - 2)
		* (levelOfDetail - 2)) + 2 * levelOfDetail * (levelOfDetail - 2);
		numberOfTriangles = (levelOfDetail - 1) * (levelOfDetail - 1) * 12;
		Vertices = new Vector3[numberOfVertices];
		Triangles = new int[numberOfTriangles * 3];
		int index = 0;
		int triIndex = 0;
		int xSide;
		bool isXPositiveSide, isXNegativeSide, isYPositiveSide, isYNegativeSide, isZPositiveSide,
		isZNegativeSide, cubeCenter;
		//TODO make this like six piece cube update with 6 forloops^2
		for (int x = 0; x < levelOfDetail; x++)
		{
			for (int y = 0; y < levelOfDetail; y++)
			{
				for (int z = 0; z < levelOfDetail; z++)
				{
					isXNegativeSide = z != levelOfDetail - 1 && y != levelOfDetail - 1 && x == 0;
					isXPositiveSide = z != levelOfDetail - 1 && y != levelOfDetail - 1 && x == levelOfDetail - 1;
					isZNegativeSide = x != levelOfDetail - 1 && y != levelOfDetail - 1 && z == 0;
					isZPositiveSide = x != levelOfDetail - 1 && y != levelOfDetail - 1 && z == levelOfDetail - 1;
					isYNegativeSide = z != levelOfDetail - 1 && x != levelOfDetail - 1 && y == 0;
					isYPositiveSide = z != levelOfDetail - 1 && x != levelOfDetail - 1 && y == levelOfDetail - 1;
					cubeCenter = (x > 0 && y > 0 && z > 0) && ((x < levelOfDetail - 1) &&
					(y < levelOfDetail - 1) && (z < levelOfDetail - 1));
					if(cubeCenter)
					{
						continue;
					}
					else
					{
						Vector3 percent = new Vector3(x * xSize, y * ySize, z * zSize) /(levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize), percent.y - (.5f * ySize), 
						percent.z - (.5f * zSize)) + this.gameObject.transform.position;
						index++;
					}
					if(!cubeCenter && isXNegativeSide)
					{
						Triangles[triIndex] = z + y * levelOfDetail;
						Triangles[triIndex + 1] = z + y * levelOfDetail + levelOfDetail + 1;
						Triangles[triIndex + 2] = z + y * levelOfDetail + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = z + y * levelOfDetail + 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					
					if(!cubeCenter && isXPositiveSide)
					{
						int xPositiveSideOffset = numberOfVertices - (levelOfDetail * levelOfDetail);
						Triangles[triIndex] = xPositiveSideOffset + z + y * levelOfDetail + 1;
						Triangles[triIndex + 1] = xPositiveSideOffset + z + y * levelOfDetail + levelOfDetail;
						Triangles[triIndex + 2] = xPositiveSideOffset + z + y * levelOfDetail + levelOfDetail + 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = xPositiveSideOffset + z + y * levelOfDetail;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					if(!cubeCenter && isZNegativeSide && x < 1 && y < 1)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail;
						Triangles[triIndex + 1] = (y + 1) * levelOfDetail;
						Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = 0;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZNegativeSide && x < 1 && y > 0)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 1] = (y + 1) * levelOfDetail;
						Triangles[triIndex + 2] = Triangles[triIndex] + 2;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = y * levelOfDetail;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZNegativeSide  && x == 1 && y < 1)
					{
						Triangles[triIndex] = 2 * levelOfDetail * levelOfDetail
						- (levelOfDetail - 2) * (levelOfDetail - 2);
						Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + levelOfDetail;
						Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = levelOfDetail * levelOfDetail;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZNegativeSide && x == 1 && y > 0)
					{
						Triangles[triIndex] = 2 * levelOfDetail * levelOfDetail -
						(levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + levelOfDetail + y * 2;
						Triangles[triIndex + 2] = 2 * levelOfDetail * levelOfDetail -
						(levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + y * 2;
						if (levelOfDetail == 3)
						{
							Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						}
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = levelOfDetail * levelOfDetail + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZNegativeSide && x > 1 && x != levelOfDetail - 2 && y < 1)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail
						- x * (levelOfDetail - 2) * (levelOfDetail - 2);
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail;
						Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = x * levelOfDetail * levelOfDetail - 
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2);
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZNegativeSide && x > 1 && x != levelOfDetail - 2 && y > 0)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail -
							(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + y * 2;
						Triangles[triIndex + 2] = Triangles[triIndex] + 2;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZNegativeSide && x == levelOfDetail - 2 && y < 1)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail - x * (levelOfDetail - 2)
						* (levelOfDetail - 2);
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail - (x - 1) * (levelOfDetail - 2)
						* (levelOfDetail - 2) + levelOfDetail;
						Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = x * levelOfDetail * levelOfDetail - (x - 1) * (levelOfDetail - 2)
						* (levelOfDetail - 2);
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZNegativeSide && x == levelOfDetail - 2 && y > 0)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail - x * (levelOfDetail - 2)
						* (levelOfDetail - 2) + y * levelOfDetail;
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail - (x - 1) * (levelOfDetail - 2)
						* (levelOfDetail - 2) + levelOfDetail + y * 2;
						Triangles[triIndex + 2] = Triangles[triIndex] + levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = x * levelOfDetail * levelOfDetail - (x - 1) * (levelOfDetail - 2)
						* (levelOfDetail - 2) + levelOfDetail + (y - 1) * 2;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					if (!cubeCenter && isZPositiveSide && x < 1 && y < 1)
					{
						xSide = levelOfDetail - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + levelOfDetail + 1;
						Triangles[triIndex + 2] = xSide + levelOfDetail;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = levelOfDetail * levelOfDetail + levelOfDetail - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x < 1 && y == levelOfDetail - 2)
					{
						xSide = (y + 1) * levelOfDetail - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = 2 * levelOfDetail * levelOfDetail -
						(levelOfDetail - 2) * (levelOfDetail - 2) - 1;
						Triangles[triIndex + 2] = (y + 2) * levelOfDetail - 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = levelOfDetail * levelOfDetail + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x < 1 && y > 0)
					{
						xSide = (y + 1) * (levelOfDetail) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + levelOfDetail + y * 2 + 1;
						Triangles[triIndex + 2] = xSide + levelOfDetail;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = levelOfDetail * levelOfDetail + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x == 1 && y == 0)
					{
						xSide = levelOfDetail * levelOfDetail + levelOfDetail - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = 2 * levelOfDetail * levelOfDetail + levelOfDetail
						- (levelOfDetail - 2) * (levelOfDetail - 2) + 1;
						Triangles[triIndex + 2] = levelOfDetail * levelOfDetail + levelOfDetail + 1;
						if (levelOfDetail == 3)
						{
							Triangles[triIndex + 1] += 1;
						}
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = 2 * levelOfDetail * levelOfDetail -
						(levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x == 1 && y == levelOfDetail - 2)
					{
						xSide = levelOfDetail * levelOfDetail + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = 3 * levelOfDetail * levelOfDetail -
						2 * (levelOfDetail - 2) * (levelOfDetail - 2) - 1;
						Triangles[triIndex + 2] = 2 * levelOfDetail * levelOfDetail
						- (levelOfDetail - 2) * (levelOfDetail - 2) - 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = 2 * levelOfDetail * levelOfDetail -
						(levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						if (levelOfDetail == 3)
						{
							Triangles[triIndex + 1] += 1;
							Triangles[triIndex + 4] += 1;
							Triangles[triIndex + 5] = Triangles[triIndex + 1];
						}
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x == 1 && y > 0)
					{
						xSide = levelOfDetail * levelOfDetail + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = 2 * levelOfDetail * levelOfDetail + levelOfDetail
						- (levelOfDetail - 2) * (levelOfDetail - 2) + 1 + y * 2;
						Triangles[triIndex + 2] = levelOfDetail * levelOfDetail + levelOfDetail + (y * 2) + 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = 2 * levelOfDetail * levelOfDetail
						- (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x > 1 && x == levelOfDetail - 2 && y < 1)
					{
						xSide = x * (levelOfDetail * levelOfDetail)
						- ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) + levelOfDetail - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = (x + 1) * levelOfDetail * levelOfDetail -
						(x * (levelOfDetail - 2) * (levelOfDetail - 2)) + (y + 2) * levelOfDetail - 1;
						Triangles[triIndex + 2] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZPositiveSide && x > 1 && x != levelOfDetail - 2 && y < 1)
					{
						xSide = x * (levelOfDetail * levelOfDetail)
						- ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) + levelOfDetail - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = xSide + (levelOfDetail * levelOfDetail) -
						(levelOfDetail - 2) * (levelOfDetail - 2) + 2;
						Triangles[triIndex + 2] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZPositiveSide && x > 1 && x != levelOfDetail - 2 && y > 0 && y != levelOfDetail - 2)
					{
						xSide = (x * (levelOfDetail * levelOfDetail)) + levelOfDetail + (y * levelOfDetail)
						- (y * (levelOfDetail - 2)) - ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + ((y + 1) * 2) - 1;
						Triangles[triIndex + 2] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + ((y + 1) * 2) - 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZPositiveSide && x > 1 && x != levelOfDetail - 2 && y == levelOfDetail - 2)
					{
						xSide = (x * (levelOfDetail * levelOfDetail)) + levelOfDetail + (y * levelOfDetail)
						- (y * (levelOfDetail - 2)) - ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = (x + 1) * levelOfDetail * levelOfDetail -
						(x * (levelOfDetail - 2) * (levelOfDetail - 2)) + 2 * levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 2] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + 2 * levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * 2) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if(!cubeCenter && isZPositiveSide && x == levelOfDetail - 2 && y > 0 && y != levelOfDetail - 2)
					{
						xSide = (x * (levelOfDetail * levelOfDetail)) + levelOfDetail + (y * levelOfDetail)
						- (y * (levelOfDetail - 2)) - ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = (x + 1) * levelOfDetail * levelOfDetail
						- (x * (levelOfDetail - 2) * (levelOfDetail - 2)) + (y + 2) * levelOfDetail - 1;
						Triangles[triIndex + 2] = xSide + 2;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * levelOfDetail) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isZPositiveSide && x == levelOfDetail - 2 && y == levelOfDetail - 2)
					{
						xSide = (x * (levelOfDetail * levelOfDetail)) + levelOfDetail + (y * levelOfDetail)
						- (y * (levelOfDetail - 2)) - ((x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2)) - 1;
						Triangles[triIndex] = xSide;
						Triangles[triIndex + 1] = numberOfVertices - 1;
						Triangles[triIndex + 2] = (x + 1) * levelOfDetail * levelOfDetail -
						(x * (levelOfDetail - 2) * (levelOfDetail - 2)) - 1;
						Triangles[triIndex + 3] = xSide;
						Triangles[triIndex + 4] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + (y * levelOfDetail) - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					if (!cubeCenter && isYNegativeSide && x < 1 && z < 1)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail + 1;
						Triangles[triIndex + 1] = 0;
						Triangles[triIndex + 2] = levelOfDetail * levelOfDetail;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isYNegativeSide && x < 1 && z > 0)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail + z + 1;
						Triangles[triIndex + 1] = z;
						Triangles[triIndex + 2] = Triangles[triIndex] - 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = z + 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isYNegativeSide && x > 0)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail - x * (levelOfDetail - 2) *
						(levelOfDetail - 2) + z + 1;
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail - (x - 1) * (levelOfDetail - 2)
						* (levelOfDetail - 2) + z;
						Triangles[triIndex + 2] = Triangles[triIndex] - 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = Triangles[triIndex + 1] + 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					
					if (!cubeCenter && isYPositiveSide && x < 1 && z < 1)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail + levelOfDetail + 
						(levelOfDetail - 2) * 2;
						Triangles[triIndex + 1] = levelOfDetail * (levelOfDetail - 1) + 1;
						Triangles[triIndex + 2] = Triangles[triIndex] + 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = (levelOfDetail - 1) * levelOfDetail;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isYPositiveSide && x < 1 && z > 0)
					{
						Triangles[triIndex] = levelOfDetail * levelOfDetail + levelOfDetail + 
						(levelOfDetail - 2) * 2 + z;
						Triangles[triIndex + 1] = levelOfDetail * (levelOfDetail - 1) + z + 1;
						Triangles[triIndex + 2] = Triangles[triIndex] + 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = levelOfDetail * (levelOfDetail - 1) + z;
						Triangles[triIndex + 5] = levelOfDetail * (levelOfDetail - 1) + z + 1;
						triIndex += 6;
					}
					else if (!cubeCenter && isYPositiveSide && x > 0 && z >= 0 && x != levelOfDetail - 2)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail + levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + (levelOfDetail - 2) * 2 + z;
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail
						+ (levelOfDetail - 2) * 2 + z + 1;
						Triangles[triIndex + 2] = Triangles[triIndex] + 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail
						+ (levelOfDetail - 2) * 2 + z;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
					else if (!cubeCenter && isYPositiveSide && x > 0 && z >= 0 && x == levelOfDetail - 2)
					{
						Triangles[triIndex] = (x + 1) * levelOfDetail * levelOfDetail -
						x * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail * (levelOfDetail - 1) + z;
						Triangles[triIndex + 1] = x * levelOfDetail * levelOfDetail -
						(x - 1) * (levelOfDetail - 2) * (levelOfDetail - 2) + levelOfDetail + 
						(levelOfDetail - 2) * 2 + z + 1;
						Triangles[triIndex + 2] = Triangles[triIndex] + 1;
						Triangles[triIndex + 3] = Triangles[triIndex];
						Triangles[triIndex + 4] = Triangles[triIndex + 1] - 1;
						Triangles[triIndex + 5] = Triangles[triIndex + 1];
						triIndex += 6;
					}
				}
			}
		}
		mesh[0].vertices = Vertices;
		mesh[0].triangles = Triangles;
		mesh[0].RecalculateNormals();
		stopStopwatch();
	}

	/**
	* \brief Create a cube out of six meshes
	* \param xSize The length of the cube on the x Axis.
	* \param ySize The length of the cube on the y Axis.
	* \param zSize The length of the cube on the z Axis.
	* \param levelOfDetail This controls how many vertices the cube will be made up of.
	*/
	public void sixPieceCubeUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject, ref mesh, ref Triangles, ref Vertices);
		primitiveObject = new GameObject[6];
		mesh = new Mesh[6];
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		numberOfVertices = levelOfDetail * levelOfDetail * 6;
		numberOfTriangles = (levelOfDetail - 1) * (levelOfDetail - 1) * 12;
		for (int i = 0; i < 6; i++)
		{
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			Vertices = new Vector3[levelOfDetail * levelOfDetail];
			Triangles = new int[(levelOfDetail - 1) * (levelOfDetail - 1) * 6];
			int triIndex = 0;
			int index;
			if(i == 0)
			{
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int y = 0; y < levelOfDetail; y++)
					{
						index =  y + x * levelOfDetail;
						Vector2 percent = new Vector2(x * xSize,y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize), percent.y - (.5f * ySize),
						(zSize * .5f)) + this.transform.position;
						if(y != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index + levelOfDetail;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index;
							Triangles[triIndex + 3] = index + levelOfDetail + 1;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 1)
			{
				triIndex = 0;
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int y = 0; y < levelOfDetail; y++)
					{
						index =  y + x * levelOfDetail;;
						Vector2 percent = new Vector2(x * xSize,y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize), percent.y - (.5f * ySize),
						-(zSize * .5f)) + this.transform.position;
						if(y != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index + levelOfDetail;
							Triangles[triIndex + 3] = index;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index + levelOfDetail + 1;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 2)
			{
				triIndex = 0;
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int z = 0; z < levelOfDetail; z++)
					{
						index =  z + x * levelOfDetail;
						Vector2 percent = new Vector2(x * xSize,z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize),(.5f * ySize),
						percent.y - (.5f * zSize)) + this.transform.position;
						if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index + levelOfDetail;
							Triangles[triIndex + 3] = index;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index + levelOfDetail + 1;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 3)
			{
				triIndex = 0;
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int z = 0; z < levelOfDetail; z++)
					{
						index =  z + x * levelOfDetail;
						Vector2 percent = new Vector2(x * xSize,z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize),-(.5f * ySize), 
						percent.y - (.5f * zSize)) + this.transform.position;
						if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index + levelOfDetail;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index;
							Triangles[triIndex + 3] = index + levelOfDetail + 1;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 4)
			{
				triIndex = 0;
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int z = 0; z < levelOfDetail; z++)
					{
						index =  z + x * levelOfDetail;
						Vector2 percent = new Vector2(x * ySize,z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(.5f * xSize, percent.x - (.5f * ySize),
						percent.y - (.5f * zSize)) + this.transform.position;
						if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index + levelOfDetail;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index;
							Triangles[triIndex + 3] = index + levelOfDetail + 1;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 5)
			{
				triIndex = 0;
				for (int x = 0; x < levelOfDetail; x++)
				{
					for (int z = 0; z < levelOfDetail; z++)
					{
						index =  z + x * levelOfDetail;
						Vector2 percent = new Vector2(x * ySize,z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(-(.5f * xSize), percent.x - (.5f * ySize),
						percent.y - (.5f * zSize)) + this.transform.position;
						if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex + 2] = index + levelOfDetail;
							Triangles[triIndex + 3] = index;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index + levelOfDetail + 1;
							triIndex += 6;
						}
					}
				}
			}
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		stopStopwatch();
	}
	
	/**
	* \brief Create a cube out of many meshes
	* \param xSize The length of the cube on the x Axis.
	* \param ySize The length of the cube on the y Axis.
	* \param zSize The length of the cube on the z Axis.
	* \param levelOfDetail This controls how many vertices the cube will be made up of.
	*/
	public void manyPieceCubeUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject,ref mesh, ref Triangles,ref Vertices);
		primitiveObject = new GameObject[(levelOfDetail - 1) * (levelOfDetail - 1) * 6];
		mesh = new Mesh[(levelOfDetail - 1) * (levelOfDetail - 1) * 6];
		numberOfVertices = (levelOfDetail - 1) * (levelOfDetail - 1) * 4 * 6;
		numberOfTriangles = (levelOfDetail - 1) * (levelOfDetail - 1) * 12;
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		int counter = 1;
		int yHeight = 0;
		for(int i = 0; i<(levelOfDetail - 1) * (levelOfDetail - 1) * 6; i++)
		{
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			Vertices = new Vector3[4];
			Triangles = new int[6];
			int index = 0;
			bool isZPositiveSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 1;
			bool isZNegativeSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 2;
			bool isXPositiveSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 3;
			bool isXNegativeSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 4;
			bool isYPositiveSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 5;
			bool isYNegativeSide = (float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) < 6;
			if(isZPositiveSide)
			{
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int y = 0; y < 2; y++)
					{
						Vector2 percent = new Vector2(x * xSize, y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x + (float)xSize * i/(levelOfDetail - 1)
						- (.5f * xSize) -  (float)yHeight * xSize, percent.y 
						+ (float)yHeight * ySize/(levelOfDetail - 1) - (.5f * ySize), zSize * .5f)
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 2;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 1;
				counter++;
			}
			else if(isZNegativeSide)
			{
				if((float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) == 1)
				{
					yHeight = 0;
					counter = 1;
				}
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int y = 0; y < 2; y++)
					{
						Vector2 percent = new Vector2(x * xSize, y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x + (float)xSize * i/(levelOfDetail - 1)
						- (.5f * xSize) - (float)yHeight * xSize - (levelOfDetail - 1) * xSize,
						percent.y + (float)yHeight * ySize/(levelOfDetail - 1) - (.5f * ySize), -zSize * .5f)
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 3;
				Triangles[1] = 2;
				Triangles[2] = 0;
				Triangles[3] = 1;
				Triangles[4] = 3;
				Triangles[5] = 0;
				counter++;
			}
			else if(isXPositiveSide)
			{
				if((float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) == 2)
				{
					yHeight = 0;
					counter = 1;
				}
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int z = 0; z < 2; z++)
				{
					for(int y = 0; y < 2; y++)
					{
						Vector2 percent = new Vector2(z * zSize, y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(xSize * .5f,percent.y + (float)yHeight * ySize/(levelOfDetail - 1)
						- (.5f * ySize), percent.x + (float)zSize * (i - (levelOfDetail - 1) * (levelOfDetail - 1) * 2)/
						(levelOfDetail - 1) - (zSize * .5f) - (float)yHeight * zSize)
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 3;
				Triangles[1] = 2;
				Triangles[2] = 0;
				Triangles[3] = 1;
				Triangles[4] = 3;
				Triangles[5] = 0;
				counter++;
			}
			else if(isXNegativeSide)
			{
				if((float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) == 3)
				{
					yHeight = 0;
					counter = 1;
				}
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int z = 0; z < 2; z++)
				{
					for(int y = 0; y < 2; y++)
					{
						Vector2 percent = new Vector2(z * zSize, y * ySize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(-(xSize * .5f),percent.y + (float)yHeight * ySize/(levelOfDetail - 1)
						- (.5f * ySize), -percent.x - (float)zSize * (i - (levelOfDetail - 1) * (levelOfDetail - 1) * 3)/
						(levelOfDetail - 1) + (zSize * .5f) + (float)(yHeight * zSize))
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 3;
				Triangles[1] = 2;
				Triangles[2] = 0;
				Triangles[3] = 1;
				Triangles[4] = 3;
				Triangles[5] = 0;
				counter++;
			}
			else if(isYPositiveSide)
			{
				if((float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) == 4)
				{
					yHeight = 0;
					counter = 1;
				}
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int z = 0; z < 2; z++)
					{
						Vector2 percent = new Vector2(x * xSize, z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - xSize * .5f + (float)xSize * (i - (levelOfDetail - 1)
						* (levelOfDetail - 1) * 4)/(levelOfDetail - 1) - yHeight * xSize, (.5f * ySize), percent.y 
						- (zSize * .5f) + (yHeight * zSize)/(levelOfDetail - 1))
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 3;
				Triangles[1] = 2;
				Triangles[2] = 0;
				Triangles[3] = 1;
				Triangles[4] = 3;
				Triangles[5] = 0;
				counter++;
			}
			else if(isYNegativeSide)
			{
				if((float)i/((levelOfDetail - 1) * (levelOfDetail - 1)) == 5)
				{
					yHeight = 0;
					counter = 1;
				}
				if(counter == levelOfDetail)
				{
					yHeight++;
					counter = 1;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int z = 0; z < 2; z++)
					{
						Vector2 percent = new Vector2(x * xSize, z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - xSize * .5f + (float)xSize * (i - (levelOfDetail - 1)
						* (levelOfDetail - 1) * 5)/(levelOfDetail - 1) - yHeight * xSize, -(.5f * ySize), percent.y 
						- (zSize * .5f) + (yHeight * zSize)/(levelOfDetail - 1))
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 2;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 1;
				counter++;
			}
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		stopStopwatch();
	}
}