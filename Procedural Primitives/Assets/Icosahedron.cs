using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icosahedron
{
	Mesh mesh;
	
	public Icosahedron(Mesh mesh)
	{
		this.mesh = mesh;
	}
	
	public void Update(float size)
	{
		Vector3[]  Vertices = new Vector3[12];
		float t = (1f + (float)Math.Sqrt(5f))/2f;
		
		//Create 12 Vertices
		Vertices[0] = new Vector3(-1, t, 0) * size;
		Vertices[1] = new Vector3(1, t, 0) * size;
		Vertices[2] = new Vector3(-1, -t, 0) * size;
		Vertices[3] = new Vector3(1, -t, 0) * size;
		Vertices[4] = new Vector3(0, -1, t) * size;
		Vertices[5] = new Vector3(0, 1, t) * size;
		Vertices[6] = new Vector3(0, -1, -t) * size;
		Vertices[7] = new Vector3(0, 1, -t) * size;
		Vertices[8] = new Vector3(t, 0, -1)* size;
		Vertices[9] = new Vector3(t, 0, 1) * size;
		Vertices[10] = new Vector3(-t, 0, -1) * size;
		Vertices[11] = new Vector3(-t, 0, 1) * size;
		
		//Create 20 Triangles
		int[] Triangles = new int[60]
		{
			0,11,5,
			0,5,1,
			0,1,7,
			0,7,10,
			0,10,11,
			1,5,9,
			5,11,4,
			11,10,2,
			10,7,6,
			7,1,8,
			3,9,4,
			3,4,2,
			3,2,6,
			3,6,8,
			3,8,9,
			4,9,5,
			2,4,11,
			6,2,10,
			8,6,7,
			9,8,1
		};
		mesh.Clear();
		mesh.vertices = Vertices;
		mesh.triangles = Triangles;
	}
}
