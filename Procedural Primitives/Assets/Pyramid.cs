using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyramid : PrimitiveBase
{
	IPrimitiveHelperMethods PHM = new PrimitiveHelperMethods();
	
	public void onePiecePyramidUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject, ref mesh, ref Triangles, ref Vertices);
		
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		
		primitiveObject = new GameObject[1];
		mesh = new Mesh[1];
		
		if(levelOfDetail > 2)
		{
			numberOfVertices = (levelOfDetail - 2) * (levelOfDetail - 2) + 
			4 * (levelOfDetail * (levelOfDetail - 1)) + 1;
			numberOfTriangles = 4 * (6 * (levelOfDetail - 1) * (levelOfDetail - 2) + 3)
			+ 6 * (levelOfDetail - 2) * (levelOfDetail - 2);
		}
		else
		{
			numberOfVertices = 5;
			numberOfTriangles = 18;
			Vertices = new Vector3[numberOfTriangles];
			Triangles = new int[numberOfTriangles];
			Vector3[] normals = new Vector3[numberOfTriangles];
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, 0);
			float halfOfX = xSize * .5f;
			float halfOfY = ySize * .5f;
			float halfOfZ = zSize * .5f;
			Vertices[0] = new Vector3(-halfOfX, -halfOfY, -halfOfZ) + this.transform.position;
			Vertices[1] = new Vector3(-halfOfX, -halfOfY, halfOfZ) + this.transform.position;
			Vertices[2] = new Vector3(halfOfX, -halfOfY, -halfOfZ) + this.transform.position;
			Vertices[3] = new Vector3(halfOfX, -halfOfY, halfOfZ) + this.transform.position;
			Vertices[4] = new Vector3(0, halfOfY, 0) + this.transform.position;
			Triangles[0] = 0;
			normals[0] = Vector3.Cross(Vertices[3] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[1] = 3;
			normals[1] = Vector3.Cross(Vertices[3] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[2] = 1;
			normals[2] = Vector3.Cross(Vertices[3] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[3] = 0;
			normals[3] = Vector3.Cross(Vertices[2] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[4] = 2;
			normals[4] = Vector3.Cross(Vertices[2] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[5] = 3;
			normals[5] = Vector3.Cross(Vertices[2] - Vertices[0], Vertices[1] - Vertices[0]).normalized;
			Triangles[6] = 0;
			normals[6] = Vector3.Cross(Vertices[4] - Vertices[0], Vertices[2] - Vertices[0]).normalized;
			Triangles[7] = 4;
			normals[7] = Vector3.Cross(Vertices[4] - Vertices[0], Vertices[2] - Vertices[0]).normalized;
			Triangles[8] = 2;
			normals[8] = Vector3.Cross(Vertices[4] - Vertices[0], Vertices[2] - Vertices[0]).normalized;
			Triangles[9] = 1;
			normals[9] = Vector3.Cross(Vertices[4] - Vertices[1], Vertices[0] - Vertices[1]).normalized;
			Triangles[10] = 4;
			normals[10] = Vector3.Cross(Vertices[4] - Vertices[1], Vertices[0] - Vertices[1]).normalized;
			Triangles[11] = 0;
			normals[11] = Vector3.Cross(Vertices[4] - Vertices[1], Vertices[0] - Vertices[1]).normalized;
			Triangles[12] = 2;
			normals[12] = Vector3.Cross(Vertices[4] - Vertices[2], Vertices[3] - Vertices[2]).normalized;
			Triangles[13] = 4;
			normals[13] = Vector3.Cross(Vertices[4] - Vertices[2], Vertices[3] - Vertices[2]).normalized;
			Triangles[14] = 3;
			normals[14] = Vector3.Cross(Vertices[4] - Vertices[2], Vertices[3] - Vertices[2]).normalized;
			Triangles[15] = 3;
			normals[15] = Vector3.Cross(Vertices[4] - Vertices[3], Vertices[1] - Vertices[3]).normalized;
			Triangles[16] = 4;
			normals[16] = Vector3.Cross(Vertices[4] - Vertices[3], Vertices[1] - Vertices[3]).normalized;
			Triangles[17] = 1;
			normals[17] = Vector3.Cross(Vertices[4] - Vertices[3], Vertices[1] - Vertices[3]).normalized;
			mesh[0].vertices = Vertices;
			mesh[0].triangles = Triangles;
			mesh[0].normals = normals;
			//mesh[0].RecalculateNormals();
			stopStopwatch();
			return;
		}
		
		Vertices = new Vector3[numberOfVertices];
		Triangles = new int[numberOfTriangles * 3];
		
		PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, 0);
		
		int index = 0;
		int triIndex = 0;
		
		Vertices[Vertices.Length-1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
		
		for(int x = 0; x < levelOfDetail; x++)
		{
			for(int z = 0; z < levelOfDetail; z++)
			{
				index = z + x * levelOfDetail;
				
				Vector2 percent = new Vector2(x * xSize,z * zSize) / (levelOfDetail - 1);
				Vertices[index] = new Vector3(percent.x - (.5f * xSize),-(.5f * ySize),
				percent.y - (.5f * zSize)) + this.transform.position;
				
				if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
				{
					Triangles[triIndex] = index + levelOfDetail;
					Triangles[triIndex + 1] = index + levelOfDetail + 1;
					Triangles[triIndex+ 2] = index;
					Triangles[triIndex + 3] = index + levelOfDetail + 1;
					Triangles[triIndex + 4] = index + 1;
					Triangles[triIndex + 5] = index;
					triIndex += 6;
				}
			}
		}
		
		index = (levelOfDetail * levelOfDetail);
		
		for(int x = 0; x < levelOfDetail; x++)
		{
			for(int y = 1; y < (levelOfDetail - 1); y++)
			{
				float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)xSize - ((float)y / ((float)levelOfDetail - 1f) * (float)xSize);
				float xPercent = ((float)x / ((float)levelOfDetail - 1f)) * baseLength;
				float zPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * zSize);
				Vertices[index] = new Vector3(xPercent - (.5f * baseLength), yPercent - (.5f * (float)ySize), zPercent - (.5f * zSize))
				 + this.transform.position;
				index++;
				if(x != levelOfDetail - 1 && y == 1)
				{
					Triangles[triIndex] = x * levelOfDetail;
					Triangles[triIndex + 1] = (levelOfDetail * levelOfDetail) + (x * (levelOfDetail - 2));
					Triangles[triIndex + 2] = (levelOfDetail * levelOfDetail) + ((x + 1) * (levelOfDetail - 2));
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = (x + 1) * levelOfDetail;
					triIndex += 6;
				}
				else if(x != levelOfDetail - 1 && y > 1)
				{
					Triangles[triIndex] = (levelOfDetail * levelOfDetail) + (x * (levelOfDetail - 2)) + (y - 2);
					Triangles[triIndex + 1] = Triangles[triIndex] + 1;
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex + 1] + levelOfDetail - 3;
					triIndex += 6;
				}
			}
		}
		
		Triangles[triIndex] = (levelOfDetail * levelOfDetail) + levelOfDetail - 3;
		Triangles[triIndex + 1] = Vertices.Length - 1;
		Triangles[triIndex + 2] = (levelOfDetail * levelOfDetail - 1) + levelOfDetail * (levelOfDetail - 2);
		triIndex += 3;
		
		for(int z = 1; z < levelOfDetail; z++)
		{
			for(int y = 1; y < (levelOfDetail - 1); y++)
			{
				float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)zSize - ((float)y / ((float)levelOfDetail - 1f) * (float)zSize);
				float zPercent = ((float)z / ((float)levelOfDetail - 1f)) * baseLength;
				float xPercent = (float)xSize - ((float)y / ((float)levelOfDetail - 1f)) * (.5f * xSize);
				Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), zPercent - (.5f * baseLength))
				 + this.transform.position;
				index++;
				if((z - 1) != levelOfDetail - 1 && y == 1)
				{
					Triangles[triIndex] = levelOfDetail * (levelOfDetail - 1) + (z - 1);
					Triangles[triIndex + 1] = (levelOfDetail * levelOfDetail) + ((levelOfDetail - 1) * (levelOfDetail - 2))
					+ (z - 1) * (levelOfDetail - 2);
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex] + 1;
					triIndex += 6;
				}
				else if((z - 1) != levelOfDetail - 1 && y > 1)
				{
					Triangles[triIndex] = (levelOfDetail * levelOfDetail) + ((levelOfDetail - 1) * (levelOfDetail - 2))
					+ (z - 1) * (levelOfDetail - 2) + (y - 2);
					Triangles[triIndex + 1] = Triangles[triIndex] + 1;
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex + 1] + levelOfDetail - 3;
					triIndex += 6;
				}
			}
		}
		
		Triangles[triIndex] = (levelOfDetail * levelOfDetail) + ((levelOfDetail - 1) * (levelOfDetail - 2))
		+ (levelOfDetail - 3);
		Triangles[triIndex + 1] = Vertices.Length - 1;
		Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail - 1) * (levelOfDetail - 2);
		triIndex += 3;
		
		for(int x = 1; x < levelOfDetail; x++)
		{
			for(int y = 1; y < (levelOfDetail - 1); y++)
			{
				float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)xSize - ((float)y / ((float)levelOfDetail - 1f) * (float)xSize);
				float xPercent = ((float)x / ((float)levelOfDetail - 1f)) * baseLength;
				float zPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * zSize);
				Vertices[index] = new Vector3((.5f * baseLength) - xPercent, yPercent - (.5f * (float)ySize),(.5f * zSize) - zPercent)
				 + this.transform.position;
				index++;
				if((x - 1) != levelOfDetail - 1 && y == 1)
				{
					Triangles[triIndex] = levelOfDetail * levelOfDetail - 1 - (x - 1) * levelOfDetail;
					Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
					(levelOfDetail - 2) * (levelOfDetail - 2) + (x - 1) * (levelOfDetail - 2);
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + (levelOfDetail - 2);
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex] - levelOfDetail;
					triIndex += 6;
				}
				else if((x - 1) != levelOfDetail - 1 && y > 1)
				{
					Triangles[triIndex] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
					(levelOfDetail - 2) * (levelOfDetail - 2) + (x - 1) * (levelOfDetail - 2) + (y - 2);
					Triangles[triIndex + 1] = Triangles[triIndex] + 1;
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex + 1] + levelOfDetail - 3;
					triIndex += 6;
				}
			}
		}
		
		Triangles[triIndex] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
		(levelOfDetail - 2) * (levelOfDetail - 2) + (levelOfDetail - 2) - 1; 
		Triangles[triIndex + 1] = Vertices.Length - 1;
		Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail - 2) * (levelOfDetail - 1);
		triIndex += 3;
		
		for(int z = 1; z < levelOfDetail; z++)
		{
			for(int y = 1; y < (levelOfDetail - 1); y++)
			{
				float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)zSize - ((float)y / ((float)levelOfDetail - 1f) * (float)zSize);
				float zPercent = ((float)z / ((float)levelOfDetail - 1f)) * baseLength;
				float xPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * xSize);
				Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), (.5f * baseLength) - zPercent)
				 + this.transform.position;
				index++;
				if((z - 1) != levelOfDetail - 1 && y == 1)
				{
					Triangles[triIndex] = levelOfDetail - z;
					Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
					(levelOfDetail - 2) * (levelOfDetail - 2) + (levelOfDetail -2) * (levelOfDetail - 1)
					+ ((z -1) * (levelOfDetail - 2));
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex] - 1;
					triIndex += 6;
				}
				else if((z - 1) != levelOfDetail && y == 1)
				{
					Triangles[triIndex] = 1;
					Triangles[triIndex + 1] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
					(levelOfDetail - 2) * (levelOfDetail - 2) + (levelOfDetail -2) * (levelOfDetail - 1)
					+ ((z -1) * (levelOfDetail - 2));
					Triangles[triIndex + 2] = levelOfDetail * levelOfDetail + (y -1);
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = 0;
				}
				else if((z - 1) != levelOfDetail - 1 && y > 1)
				{
					Triangles[triIndex] = levelOfDetail * levelOfDetail + (levelOfDetail - 2) * levelOfDetail + 
					(levelOfDetail - 2) * (levelOfDetail - 2) + (levelOfDetail -2) * (levelOfDetail - 1)
					+ ((z -1) * (levelOfDetail - 2)) + (y -2);
					Triangles[triIndex + 1] = Triangles[triIndex] + 1;
					Triangles[triIndex + 2] = Triangles[triIndex + 1] + levelOfDetail - 2;
					Triangles[triIndex + 3] = Triangles[triIndex];
					Triangles[triIndex + 4] = Triangles[triIndex + 2];
					Triangles[triIndex + 5] = Triangles[triIndex + 1] + levelOfDetail - 3;
					triIndex += 6;
				}
			}
		}
		Triangles[triIndex] = levelOfDetail * levelOfDetail - 1 + 3 * ((levelOfDetail - 2) * (levelOfDetail - 1)) + 
		+ levelOfDetail - 2;
		Triangles[triIndex + 1] = Vertices.Length - 1;
		Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail - 1) * (levelOfDetail - 2);
		triIndex += 3;
		
		mesh[0].vertices = Vertices;
		mesh[0].triangles = Triangles;
		mesh[0].RecalculateNormals();
		stopStopwatch();
	}
	
	public void fivePiecePyramidUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject, ref mesh, ref Triangles, ref Vertices);
		
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		
		primitiveObject = new GameObject[5];
		mesh = new Mesh[5];
		
		numberOfVertices = levelOfDetail * levelOfDetail + (4 * ((levelOfDetail * (levelOfDetail - 1)) + 1));
		numberOfTriangles = 4 * ((6 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 3) + (levelOfDetail - 1) * (levelOfDetail - 1) * 6;
		
		for(int i = 0; i < 5; i++)
		{
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			
			
			int triIndex = 0;
			int index = 0;
			
			if(i == 0)
			{
				Vertices = new Vector3[levelOfDetail * levelOfDetail];
				Triangles = new int[6 * (levelOfDetail - 1) * (levelOfDetail - 1)];
				for(int x = 0; x < levelOfDetail; x++)
				{
					for(int z = 0; z < levelOfDetail; z++)
					{
						index = z + x * levelOfDetail;
						
						Vector2 percent = new Vector2(x * xSize,z * zSize) / (levelOfDetail - 1);
						Vertices[index] = new Vector3(percent.x - (.5f * xSize),-(.5f * ySize),
						percent.y - (.5f * zSize)) + this.transform.position;
						
						if(z != levelOfDetail - 1 && x != levelOfDetail - 1)
						{
							Triangles[triIndex] = index + levelOfDetail;
							Triangles[triIndex + 1] = index + levelOfDetail + 1;
							Triangles[triIndex+ 2] = index;
							Triangles[triIndex + 3] = index + levelOfDetail + 1;
							Triangles[triIndex + 4] = index + 1;
							Triangles[triIndex + 5] = index;
							triIndex += 6;
						}
					}
				}
			}
			else if(i == 1)
			{
				Vertices = new Vector3[levelOfDetail * (levelOfDetail - 1) + 1];
				Triangles = new int[6 * (levelOfDetail - 1) * (levelOfDetail - 2) + 3];
				Vertices[Vertices.Length-1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				for(int x = 0; x < levelOfDetail; x++)
				{
					for(int y = 0; y < (levelOfDetail - 1); y++)
					{
						float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)xSize - ((float)y / ((float)levelOfDetail - 1f) * (float)xSize);
						float xPercent = ((float)x / ((float)levelOfDetail - 1f)) * baseLength;
						float zPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * zSize);
						Vertices[index] = new Vector3(xPercent - (.5f * baseLength), yPercent - (.5f * (float)ySize), zPercent - (.5f * zSize))
						 + this.transform.position;
						index++;
						if(x < levelOfDetail - 1 && y < levelOfDetail - 2)
						{
							Triangles[triIndex] = x * (levelOfDetail - 1) + y;
							Triangles[triIndex + 1] = Triangles[triIndex] + 1;
							Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail);
							Triangles[triIndex + 3] = Triangles[triIndex];
							Triangles[triIndex + 4] = Triangles[triIndex + 2];
							Triangles[triIndex + 5] = Triangles[triIndex] + (levelOfDetail - 1);
							triIndex += 6;
						}
					}
				}
				Triangles[triIndex] = levelOfDetail - 2;
				Triangles[triIndex + 1] = Vertices.Length - 1;
				Triangles[triIndex + 2] = levelOfDetail * (levelOfDetail - 1) - 1;
			}
			else if(i == 2)
			{
				Vertices = new Vector3[levelOfDetail * (levelOfDetail - 1) + 1];
				Triangles = new int[6 * (levelOfDetail - 1) * (levelOfDetail - 2) + 3];
				Vertices[Vertices.Length-1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				for(int z = 0; z < levelOfDetail; z++)
				{
					for(int y = 0; y < (levelOfDetail - 1); y++)
					{
						float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)zSize - ((float)y / ((float)levelOfDetail - 1f) * (float)zSize);
						float zPercent = ((float)z / ((float)levelOfDetail - 1f)) * baseLength;
						float xPercent = (float)xSize - ((float)y / ((float)levelOfDetail - 1f)) * (.5f * xSize);
						Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), zPercent - (.5f * baseLength))
						 + this.transform.position;
						index++;
						if(z < levelOfDetail - 1 && y < levelOfDetail - 2)
						{
							Triangles[triIndex] = z * (levelOfDetail - 1) + y;
							Triangles[triIndex + 1] = Triangles[triIndex] + 1;
							Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail);
							Triangles[triIndex + 3] = Triangles[triIndex];
							Triangles[triIndex + 4] = Triangles[triIndex + 2];
							Triangles[triIndex + 5] = Triangles[triIndex] + (levelOfDetail - 1);
							triIndex += 6;
						}
					}
				}
				Triangles[triIndex] = levelOfDetail - 2;
				Triangles[triIndex + 1] = Vertices.Length - 1;
				Triangles[triIndex + 2] = levelOfDetail * (levelOfDetail - 1) - 1;
			}
			else if(i == 3)
			{
				Vertices = new Vector3[levelOfDetail * (levelOfDetail - 1) + 1];
				Triangles = new int[6 * (levelOfDetail - 1) * (levelOfDetail - 2) + 3];
				Vertices[Vertices.Length-1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				for(int x = 0; x < levelOfDetail; x++)
				{
					for(int y = 0; y < (levelOfDetail - 1); y++)
					{
						float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)xSize - ((float)y / ((float)levelOfDetail - 1f) * (float)xSize);
						float xPercent = ((float)x / ((float)levelOfDetail - 1f)) * baseLength;
						float zPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * zSize);
						Vertices[index] = new Vector3((.5f * baseLength) - xPercent, yPercent - (.5f * (float)ySize),(.5f * zSize) - zPercent)
						 + this.transform.position;
						index++;
						if(x < levelOfDetail - 1 && y < levelOfDetail - 2)
						{
							Triangles[triIndex] = x * (levelOfDetail - 1) + y;
							Triangles[triIndex + 1] = Triangles[triIndex] + 1;
							Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail);
							Triangles[triIndex + 3] = Triangles[triIndex];
							Triangles[triIndex + 4] = Triangles[triIndex + 2];
							Triangles[triIndex + 5] = Triangles[triIndex] + (levelOfDetail - 1);
							triIndex += 6;
						}
					}
				}
				Triangles[triIndex] = levelOfDetail - 2;
				Triangles[triIndex + 1] = Vertices.Length - 1;
				Triangles[triIndex + 2] = levelOfDetail * (levelOfDetail - 1) - 1;
			}
			else if(i == 4)
			{
				Vertices = new Vector3[levelOfDetail * (levelOfDetail - 1) + 1];
				Triangles = new int[6 * (levelOfDetail - 1) * (levelOfDetail - 2) + 3];
				Vertices[Vertices.Length-1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				for(int z = 0; z < levelOfDetail; z++)
				{
					for(int y = 0; y < (levelOfDetail - 1); y++)
					{
						float yPercent = ((float)y * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)zSize - ((float)y / ((float)levelOfDetail - 1f) * (float)zSize);
						float zPercent = ((float)z / ((float)levelOfDetail - 1f)) * baseLength;
						float xPercent = ((float)y / ((float)levelOfDetail - 1f)) * (.5f * xSize);
						Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), (.5f * baseLength) - zPercent)
						+ this.transform.position;
						index++;
						if(z < levelOfDetail - 1 && y < levelOfDetail - 2)
						{
							Triangles[triIndex] = z * (levelOfDetail - 1) + y;
							Triangles[triIndex + 1] = Triangles[triIndex] + 1;
							Triangles[triIndex + 2] = Triangles[triIndex] + (levelOfDetail);
							Triangles[triIndex + 3] = Triangles[triIndex];
							Triangles[triIndex + 4] = Triangles[triIndex + 2];
							Triangles[triIndex + 5] = Triangles[triIndex] + (levelOfDetail - 1);
							triIndex += 6;
						}
					}
				}
				Triangles[triIndex] = levelOfDetail - 2;
				Triangles[triIndex + 1] = Vertices.Length - 1;
				Triangles[triIndex + 2] = levelOfDetail * (levelOfDetail - 1) - 1;
			}
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		stopStopwatch();
	}
	
	public void manyPiecePyramidUpdate(float xSize, float ySize, float zSize, int levelOfDetail)
	{
		startStopwatch();
		PHM.deleteOldPrimitive(ref primitiveObject, ref mesh, ref Triangles, ref Vertices);
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;
		this.levelOfDetail = levelOfDetail;
		int numOfFaces = (levelOfDetail - 1) * (levelOfDetail - 1) + (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 4;
		if(levelOfDetail == 2)
			numberOfVertices = levelOfDetail * levelOfDetail + (4 * levelOfDetail * (levelOfDetail - 1)) + 4;
		else
			numberOfVertices = levelOfDetail * levelOfDetail + (4 * levelOfDetail * (levelOfDetail - 1)) + 12;
		numberOfTriangles = 6 * (levelOfDetail - 1) * (levelOfDetail - 1) + (6 * (4 * (levelOfDetail - 1) * (levelOfDetail - 2)))
		+ 12;
		primitiveObject = new GameObject[numOfFaces];
		mesh = new Mesh[numOfFaces];
		int yHeight = 0;
		int horizontalLength = 0;
		
		for(int i = 0; i < 4 * (levelOfDetail - 1) * (levelOfDetail - 2); i++)
		{
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			Vertices = new Vector3[4];
			Triangles = new int[6];
			int index = 0;
			bool isZNegativeSide = (float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) < 1;
			bool isXPositivieSide = (float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) < 2;
			bool isZPositiveSide = (float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) < 3;
			bool isXNegativeSide = (float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) < 4;
			if(isZNegativeSide)
			{
				if(yHeight == levelOfDetail - 2)
				{
					horizontalLength++;
					yHeight = 0;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int y = 0; y < 2; y++)
					{
						float yPercent = ((yHeight + (float)y) * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)xSize - (((float)y + yHeight) / ((float)levelOfDetail - 1f) * (float)xSize);
						float xPercent = ((horizontalLength + (float)x) / ((float)levelOfDetail - 1f)) * baseLength;
						float zPercent = ((yHeight + (float)y) / ((float)levelOfDetail - 1f)) * (.5f * zSize);
						Vertices[index] = new Vector3(xPercent - (.5f * baseLength), yPercent - (.5f * (float)ySize), zPercent - (.5f * zSize))
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 2;
				yHeight++;
			}
			else if(isXPositivieSide)
			{
				if((float)i / ((levelOfDetail - 1) * (levelOfDetail -2)) == 1)
				{
					index = 0;
					yHeight = 0;
					horizontalLength = 0;
				}
				if(yHeight == levelOfDetail - 2)
				{
					horizontalLength++;
					yHeight = 0;
				}
				for(int z = 0; z < 2; z++)
				{
					for(int y = 0; y < 2; y++)
					{
						float yPercent = ((yHeight + (float)y) * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)zSize - (((float)y + yHeight) / ((float)levelOfDetail - 1f) * (float)zSize);
						float zPercent = ((horizontalLength + (float)z) / ((float)levelOfDetail - 1f)) * baseLength;
						float xPercent = (float)xSize - (((float)y + yHeight) / ((float)levelOfDetail - 1f)) * (.5f * xSize);
						Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), zPercent - (.5f * baseLength))
						 + this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 2;
				yHeight++;
			}
			else if(isZPositiveSide)
			{
				if((float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) == 2)
				{
					index = 0;
					yHeight = 0;
					horizontalLength = 0;
				}
				if(yHeight == levelOfDetail - 2)
				{
					yHeight = 0;
					horizontalLength++;
				}
				for(int x = 0; x < 2; x++)
				{
					for(int y = 0; y < 2; y++)
					{
						float yPercent = ((yHeight + (float)y) * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)xSize - (((float)y + yHeight)/ ((float)levelOfDetail - 1f) * (float)xSize);
						float xPercent = ((horizontalLength + (float)x) / ((float)levelOfDetail - 1f)) * baseLength;
						float zPercent = ((yHeight + (float)y) / ((float)levelOfDetail - 1f)) * (.5f * zSize);
						Vertices[index] = new Vector3((.5f * baseLength) - xPercent, yPercent - (.5f * (float)ySize),(.5f * zSize) - zPercent)
						 + this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 2;
				yHeight++;
			}
			else if(isXNegativeSide)
			{
				if((float)i / ((levelOfDetail - 1) * (levelOfDetail - 2)) == 3)
				{
					index = 0;
					yHeight = 0;
					horizontalLength = 0;
				}
				if(yHeight == levelOfDetail - 2)
				{
					horizontalLength++;
					yHeight = 0;
				}
				for(int z = 0; z < 2; z++)
				{
					for(int y = 0; y < 2; y++)
					{
						float yPercent = ((yHeight + (float)y) * (float)ySize) / ((float)levelOfDetail - 1f);
						float baseLength = (float)zSize - (((float)y + yHeight) / ((float)levelOfDetail - 1f) * (float)zSize);
						float zPercent = ((horizontalLength + (float)z) / ((float)levelOfDetail - 1f)) * baseLength;
						float xPercent = (((float)y + yHeight) / ((float)levelOfDetail - 1f)) * (.5f * xSize);
						Vertices[index] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), (.5f * baseLength) - zPercent)
						+ this.transform.position;
						index++;
					}
				}
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 3;
				Triangles[3] = 0;
				Triangles[4] = 3;
				Triangles[5] = 2;
				yHeight++;
			}
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		for(int i = (4 * (levelOfDetail - 1) * (levelOfDetail - 2)); i < (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 4; i++)
		{
			Vertices = new Vector3[3];
			Triangles = new int[3];
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			if(i == (4 * (levelOfDetail - 1) * (levelOfDetail - 2)))
			{
				float yPercent = ((levelOfDetail - 2) * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)xSize - ((levelOfDetail - 2) / ((float)levelOfDetail - 1f) * (float)xSize);
				float zPercent = ((levelOfDetail - 2) / ((float)levelOfDetail - 1f)) * (.5f * zSize);
				Vertices[0] = new Vector3(-(.5f * baseLength), yPercent - (.5f * (float)ySize), zPercent - (.5f * zSize))
				+ this.transform.position;
				Vertices[1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				yPercent = ((levelOfDetail - 2) * (float)ySize) / ((float)levelOfDetail - 1f);
				baseLength = (float)xSize - ((levelOfDetail - 2) / ((float)levelOfDetail - 1f) * (float)xSize);
				Vertices[2] = new Vector3(.5f * baseLength, yPercent - (.5f * (float)ySize), zPercent - (.5f * zSize))
				+ this.transform.position;
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 2;
			}
			else if(i == (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 1)
			{
				float yPercent = ((float)(levelOfDetail - 2) * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)zSize - ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f) * (float)zSize);
				float xPercent = (float)xSize - ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f)) * (.5f * xSize);
				Vertices[0] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize),-(.5f * baseLength))
				+ this.transform.position;
				Vertices[1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				Vertices[2] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize),(.5f * baseLength))
				+ this.transform.position;
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 2;
			}
			else if(i == (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 2)
			{
				float yPercent = ((float)(levelOfDetail - 2) * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)xSize - ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f) * (float)xSize);
				float zPercent = ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f)) * (.5f * zSize);
				Vertices[0] = new Vector3((.5f * baseLength), yPercent - (.5f * (float)ySize),(.5f * zSize) - zPercent)
				+ this.transform.position;
				Vertices[1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				Vertices[2] = new Vector3(-(.5f * baseLength), yPercent - (.5f * (float)ySize),(.5f * zSize) - zPercent)
				+ this.transform.position;
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 2;
			}
			else if(i == (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 3)
			{
				float yPercent = ((float)(levelOfDetail - 2) * (float)ySize) / ((float)levelOfDetail - 1f);
				float baseLength = (float)zSize - ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f) * (float)zSize);
				float xPercent = ((float)(levelOfDetail - 2) / ((float)levelOfDetail - 1f)) * (.5f * xSize);
				Vertices[0] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), (.5f * baseLength))
				+ this.transform.position;
				Vertices[1] = new Vector3(0, ySize - (.5f * ySize), 0) + this.transform.position;
				Vertices[2] = new Vector3(xPercent - (.5f * xSize), yPercent - (.5f * (float)ySize), -(.5f * baseLength))
				+ this.transform.position;
				Triangles[0] = 0;
				Triangles[1] = 1;
				Triangles[2] = 2;
			}
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		int vertIndex = 0;
		yHeight = 0;
		int counter = 0;
		for(int i = (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + 4;
		i < (4 * (levelOfDetail - 1) * (levelOfDetail - 2)) + ((levelOfDetail - 1) * (levelOfDetail - 1) + 4); i++)
		{
			PHM.createNewPrimitive(ref primitiveObject, ref mesh, transform, i);
			Vertices = new Vector3[4];
			Triangles = new int[6];
			if(counter == levelOfDetail - 1)
			{
				yHeight += 1;
				counter = 0;
			}
			for(int x = 0; x < 2; x++)
			{
				for(int z = 0; z < 2; z++)
				{
					Vector2 percent = new Vector2((counter + x) * xSize,(yHeight + z) * zSize) / (levelOfDetail - 1);
					Vertices[vertIndex] = new Vector3(percent.x - (.5f * xSize),-(.5f * ySize),
					percent.y - (.5f * zSize)) + this.transform.position;
					vertIndex++;
				}
			}
			Triangles[0] = 0;
			Triangles[1] = 2;
			Triangles[2] = 3;
			Triangles[3] = 0;
			Triangles[4] = 3;
			Triangles[5] = 1;
			vertIndex = 0;
			counter++;
			mesh[i].vertices = Vertices;
			mesh[i].triangles = Triangles;
			mesh[i].RecalculateNormals();
		}
		stopStopwatch();
	}
}